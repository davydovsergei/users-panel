import React, { Suspense } from 'react';
import { BrowserRouter as Router, Switch } from 'react-router-dom';
import { ScrollToTop } from './components';
import { Loader } from './ui';
import { renderRoutes } from './Routes';

import './App.css';

function App() {
  return (
    <div className="App">
      <Router>
        <ScrollToTop>
          <Suspense fallback={<Loader loading />}>
            <Switch>{renderRoutes()}</Switch>
          </Suspense>
        </ScrollToTop>
      </Router>
    </div>
  );
}

export default App;
