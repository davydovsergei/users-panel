import { usersApi } from '../api';

export const fetchUsers = () => ({ type: 'FETCH_USERS' });

export const createUser = (user) => usersApi.create(user);

export const updateUser = (id, user) => usersApi.update(id, user);

export const deleteUser = (id) => ({ type: 'DELETE_USER', id });
