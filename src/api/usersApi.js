import { request } from '../lib/request';

const getAll = () => request('GET', 'users');

const create = (user) => request('POST', 'users', { body: user });

const update = (id, user) => request('PUT', `user/${id}`, { body: user });

const remove = (id) => request('DELETE', `user/${id}`);

export const usersApi = { getAll, create, update, remove };
