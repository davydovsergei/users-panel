import React, { useEffect, useState, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { Pagination } from '../../ui';
import { User } from './User';
import { fetchUsers } from '../../actions';

const PER_PAGE = 5;

export const Users = () => {
  const dispatch = useDispatch();
  const users = useSelector((state) => state.users);

  const [currentPage, setCurrentPage] = useState(1);

  useEffect(() => {
    dispatch(fetchUsers());
  }, []);

  const totalPages = Math.ceil(users.length / PER_PAGE);

  const startIndex = (currentPage - 1) * PER_PAGE;
  const rangeUsers = users.slice(startIndex, startIndex + PER_PAGE);

  useEffect(() => {
    if (currentPage !== 1 && rangeUsers.length === 0) {
      setCurrentPage(currentPage - 1);
    }
  }, [rangeUsers, currentPage]);

  const handlePageClick = useCallback((page) => setCurrentPage(page), []);

  return (
    <>
      <table className="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Surname</th>
            <th scope="col">Desc</th>
            <th />
            <th />
          </tr>
        </thead>
        <tbody>
          {rangeUsers.map((user) => (
            <User key={user.id} {...user} />
          ))}
        </tbody>
      </table>
      <Pagination totalPages={totalPages} page={currentPage} onPageClick={handlePageClick} />
    </>
  );
};
