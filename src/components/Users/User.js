import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { fetchUsers, deleteUser } from '../../actions';
import { Write, Read } from './modes';

export const User = ({ id, ...user }) => {
  const dispatch = useDispatch();

  const [isEdit, setIsEdit] = useState(false);

  const handleEdit = () => {
    setIsEdit(!isEdit);
  };

  const handleSave = () => {
    dispatch(fetchUsers());
    handleEdit();
  };

  const handleDelete = () => {
    dispatch(deleteUser(id));
  };

  return (
    <tr>
      <th scope="row">{id}</th>
      {isEdit ? (
        <Write id={id} {...user} onSave={handleSave} onCancel={handleEdit} />
      ) : (
        <Read {...user} onEdit={handleEdit} onDelete={handleDelete} />
      )}
    </tr>
  );
};
