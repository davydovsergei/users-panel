import React, { useState } from 'react';

import { updateUser } from '../../../actions';
import { Input, ButtonSuccess, ButtonLight, Error } from '../../../ui';

export const Write = ({ id, name, surname, desc, onSave, onCancel }) => {
  const [inputs, setInputs] = useState({ name, surname, desc });
  const [errors, setErrors] = useState({});

  const handleChange = (name) => ({ target }) => {
    setInputs({
      ...inputs,
      [name]: target.value,
    });
  };

  const handleSave = () => {
    updateUser(id, inputs).then((res) => {
      if (res?.errors) {
        setErrors(res.errors.errors);
      } else {
        setErrors({});
        onSave();
      }
    });
  };

  return (
    <>
      <td>
        <Input value={inputs.name} name="name" onChange={handleChange('name')} />
        {errors.name && <Error>{errors.name}</Error>}
      </td>
      <td>
        <Input value={inputs.surname} name="surname" onChange={handleChange('surname')} />
        {errors.surname && <Error>{errors.surname}</Error>}
      </td>
      <td>
        <Input value={inputs.desc} name="desc" onChange={handleChange('desc')} />
        {errors.desc && <Error>{errors.desc}</Error>}
      </td>
      <td>
        <ButtonSuccess onClick={handleSave}>Save</ButtonSuccess>
      </td>
      <td>
        <ButtonLight onClick={onCancel}>Cancel</ButtonLight>
      </td>
    </>
  );
};
