import React from 'react';

import { ButtonWarning, ButtonDanger } from '../../../ui';

export const Read = ({ name, surname, desc, onEdit, onDelete }) => (
  <>
    <td>{name}</td>
    <td>{surname}</td>
    <td>{desc}</td>
    <td>
      <ButtonWarning onClick={onEdit}>Edit</ButtonWarning>
    </td>
    <td>
      <ButtonDanger onClick={onDelete}>Delete</ButtonDanger>
    </td>
  </>
);
