import React, { useState } from 'react';
import { Form, FormGroup, Input, Textarea, ButtonPrimary, Error, Alert } from '../../ui';
import { createUser } from '../../actions';

const initInputs = {
  name: '',
  surname: '',
  desc: '',
};

export const CreateUser = () => {
  const [inputs, setInputs] = useState(initInputs);
  const [errors, setErrors] = useState({});
  const [isSave, setIsSave] = useState(false);

  const handleChange = (name) => ({ target }) => {
    setInputs({
      ...inputs,
      [name]: target.value,
    });
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    createUser(inputs).then((res) => {
      if (res?.errors) {
        setErrors(res.errors.errors);
      } else {
        setInputs(initInputs);
        setErrors({});
        setIsSave(true);
      }
    });
  };

  console.log(errors);

  return (
    <Form onSubmit={handleSubmit}>
      <FormGroup label="Your name">
        <Input value={inputs.name} name="name" onChange={handleChange('name')} />
        {errors.name && <Error>{errors.name}</Error>}
      </FormGroup>
      <FormGroup label="Your surname">
        <Input value={inputs.surname} name="surname" onChange={handleChange('surname')} />
        {errors.surname && <Error>{errors.surname}</Error>}
      </FormGroup>
      <FormGroup label="Your description">
        <Textarea value={inputs.desc} name="desc" onChange={handleChange('desc')} />
        {errors.desc && <Error>{errors.desc}</Error>}
      </FormGroup>
      <ButtonPrimary type="submit">Create user</ButtonPrimary>
      {isSave && <Alert style={{ margin: '20px 0 0 0' }}>User was successfully created!</Alert>}
    </Form>
  );
};
