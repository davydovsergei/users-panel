import React from 'react';
import { Link } from 'react-router-dom';
import { CreateUser } from '../../components';

const Create = () => {
  return (
    <>
      <h1>Create user</h1>
      <p>
        <Link to="/" className="btn btn-primary">
          Users
        </Link>
      </p>
      <CreateUser />
    </>
  );
};

export default Create;
