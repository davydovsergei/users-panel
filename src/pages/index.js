import React from 'react';

const Users = React.lazy(() => import('./users'));
const Create = React.lazy(() => import('./create'));

export const routes = [
  {
    path: '/',
    exact: true,
    component: Users,
  },
  {
    path: '/create',
    component: Create,
  },
];
