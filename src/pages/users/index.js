import React from 'react';

import { Link } from 'react-router-dom';

import { Users as UsersComponent } from '../../components';

const Users = () => {
  return (
    <>
      <h1>Users</h1>
      <p>
        <Link to="/create" className="btn btn-primary">
          Create
        </Link>
      </p>
      <UsersComponent />
    </>
  );
};

export default Users;
