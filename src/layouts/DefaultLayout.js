import React, { Suspense } from 'react';

import { Main, Loader } from '../ui';

export const DefaultLayout = ({ children }) => {
  return (
    <div className="wrapper">
      <Main>
        <Suspense fallback={<Loader />}>{children}</Suspense>
      </Main>
    </div>
  );
};
