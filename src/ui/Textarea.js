import React from 'react';

export const Textarea = ({ type = 'text', rows = '3', ...props }) => (
  <textarea className="form-control" rows={rows} {...props} />
);
