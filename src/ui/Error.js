import React from 'react';

export const Error = ({ children }) => (
  <div style={{ color: 'red', fontSize: '70%' }}>{children}</div>
);
