import React from 'react';

export const Input = ({ type = 'text', ...props }) => (
  <input type={type} className="form-control" {...props} />
);
