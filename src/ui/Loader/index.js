import React from 'react';

import './Loader.scss';

export const Loader = ({ loading = true }) =>
  loading && (
    <div className="loader-container">
      <div className="lds-ring">
        <div></div>
        <div></div>
        <div></div>
        <div></div>
      </div>
    </div>
  );
