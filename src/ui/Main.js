import React from 'react';

export const Main = ({ children }) => (
  <div className="main">
    <div className="container">{children}</div>
  </div>
);
