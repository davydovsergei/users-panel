import React from 'react';

export const Alert = ({ children, ...props }) => (
  <div className="alert alert-success" {...props}>
    {children}
  </div>
);
