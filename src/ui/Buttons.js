import React from 'react';

export const ButtonPrimary = ({ children, type = 'button', ...props }) => (
  <button type={type} className="btn btn-primary" {...props}>
    {children}
  </button>
);

export const ButtonSuccess = ({ children, type = 'button', ...props }) => (
  <button type={type} className="btn btn-success" {...props}>
    {children}
  </button>
);

export const ButtonWarning = ({ children, type = 'button', ...props }) => (
  <button type={type} className="btn btn-warning" {...props}>
    {children}
  </button>
);

export const ButtonDanger = ({ children, type = 'button', ...props }) => (
  <button type={type} className="btn btn-danger" {...props}>
    {children}
  </button>
);

export const ButtonLight = ({ children, type = 'button', ...props }) => (
  <button type={type} className="btn btn-light" {...props}>
    {children}
  </button>
);
