import React, { useState, useEffect, useCallback } from 'react';

export const Pagination = ({ totalPages, page, onPageClick }) => {
  const [currentPage, setCurrentPage] = useState(page);
  const pagesArray = [...Array(totalPages).keys()];

  const handlePageClick = useCallback(
    (newPage) => {
      if (newPage === currentPage) return;
      setCurrentPage(newPage);
      onPageClick(newPage);
    },
    [currentPage, onPageClick]
  );

  const firstPage = (
    <li className="page-item">
      <button className="page-link">1</button>
    </li>
  );

  const renderPages = pagesArray.map((el) => {
    const realPage = el + 1;
    return (
      <li
        key={realPage}
        className={realPage === currentPage ? 'page-item active' : 'page-item'}
        onClick={() => handlePageClick(realPage)}
      >
        <button className="page-link">{realPage}</button>
      </li>
    );
  });

  useEffect(() => {
    setCurrentPage(page);
  }, [page]);

  return (
    <ul className="pagination">
      <li className="page-item">
        <button className="page-link" onClick={() => handlePageClick(1)}>
          First
        </button>
      </li>
      {!totalPages ? firstPage : renderPages}
      <li className="page-item">
        <button className="page-link" onClick={() => handlePageClick(totalPages)}>
          Last
        </button>
      </li>
    </ul>
  );
};
