import { call, takeEvery, all, put } from 'redux-saga/effects';

import { usersApi } from '../api';

export function* fetchUsers() {
  try {
    const users = yield call(usersApi.getAll);
    yield put({ type: 'FETCH_USERS_SUCCESS', users });
  } catch (error) {
    console.log('fetchUsers error: ', error);
  }
}

function* watchFetchUsers() {
  yield takeEvery('FETCH_USERS', fetchUsers);
}

export function* deleteUser({ id }) {
  try {
    yield call(usersApi.remove, id);
    yield put({ type: 'FETCH_USERS' });
  } catch (error) {
    console.log('deleteUser error:', error.message);
  }
}

function* watchDeleteUser() {
  yield takeEvery('DELETE_USER', deleteUser);
}

export default function* rootSaga() {
  yield all([watchFetchUsers(), watchDeleteUser()]);
}
